#include<stdio.h>

int main(void)
{
	char character;

	printf("Please enter a character: ");
	scanf("%c", &character);

	if(character=='a' || character=='A' || character=='e' || character=='E' || character=='i' || character=='I' || character=='o' || character=='O' || character=='u' || character=='U')
	{
		printf("This is a Vowel.\n");
	}
	else
	{
		printf("This is a Consonant.\n");
	}

	return 0;
}