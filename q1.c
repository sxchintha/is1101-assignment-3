#include<stdio.h>

int main() 
{
	float no;
	
	printf("Please enter a number: ");
	scanf("%f", &no);

	if (no==0)
	{
		printf("This is Zero.\n");
	}
	else if (no<0)
	{
		printf("This is a negative number.\n");
	}
	else
	{
		printf("This is a positive number.\n");
	}

	return 0;
}
